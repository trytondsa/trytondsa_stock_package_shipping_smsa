******
Design
******

The *Stock Package Shipping SMSA Module* introduces some new concepts.


Carrier
=======

When the *Stock Package Shipping SMSA Module* is activated, carrier can be
configured to use the "SMSA Express" shipping service.
When this configuration is chosen, carrier gains some extra properties like the
SMSA service type.

SMSA Credential
==============

A *SMSA Credential* stores the credentials to communicate with the SMSA APIs.

The first matching credential is used for carrier with "SMSA Express" shipping service.


